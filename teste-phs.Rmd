---
title: "teste-phs"
author: "Pablo Silva"
date: "12 de junho de 2018"
output:
  word_document:
    reference_docx: manuscript/sources/template.docx

# Para o estilo de citação desejado, use a função `baixar_csl.R` contida na pasta r-scripts
csl: manuscript/sources/ecology-letters.csl
bibliography:
- manuscript/sources/library.bib
- manuscript/sources/installed-r-packages.bib
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

